import { Component, OnInit ,Input , HostBinding} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
	//@Input() nombre : string;
	@Input() destino : DestinoViaje;
	@HostBinding('attr.class') cssClass = 'col-md-4'; 
	//tener una vinculacion directa de un atributo priedad string de nuestro componente se vincula directamente a un atributo del tag con el cual se  envolviendo el contenido del componente
  constructor() {
  //this.nombre = 'nombre por defecto'; // Agregamos una variable
   }
  ngOnInit(): void {
  }

}
